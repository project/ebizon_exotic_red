<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
   <head>
<?php global $base_url;?>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
   
  </head>
  <body>
    <div id="layout">
   

      <div id="header">
       
        	<div id="logo">
              
        <?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          if ($site_slogan) {
            $site_fields[] = check_plain($site_slogan);
          }
          $site_title = implode(' ', $site_fields);
          $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            print '<h1><a href="'. check_url($base_path) .'" title="'. $site_title .'">';
            if ($logo) {
              //print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print $site_html .'</a></h1>';
          }
        ?>
       
        </div>
        <hr class="noscreen"/>   
        
        <div id="nav" class="box">
        <ul> <li><?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?></li></ul>
        <hr class="noscreen"/>
      </div> 
 
      </div>
  
      <hr class="noscreen"/>

	<div id="container" class="box"> 
              
              	<div id="panel-left" class="box panel">
	            <div class="in">
                      <?php print $sidebar_left;?>
                     </div>
                </div>
                

	  <div id="obsah" class="contents box">
	      <?php print $messages;?>
              <?php print $breadcrumb; ?>
          <div class="in">
              <?php print $tabs;?>
		<?php print $content;?>
 
          </div>
        </div>
   

      
      </div>
     
      <div id="footer"> 
        <span class="f-left">Copyright (c) 2008 Ebizon Technologies .</span>
        <span class="f-right">All rights reserved. Design by <a href="http://www.ebizontek.com/">Ebizon Technologies</a>.</span>
      </div>
    </div>

  </body></html>
